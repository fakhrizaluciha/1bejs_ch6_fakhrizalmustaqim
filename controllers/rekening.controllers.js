const fs = require('fs');
const { rekening } = require('../models')
const model = require('../models')

const getAllRekening = async (req, res) => {
    let { page, row } = req.query
    page -= 1
    const options = {
        attributes: ['id', 'pin', 'cabang_bank_id', 'saldo'],
        include: [{
            model: model.cabang_bank,
            attributes: ['nama_cabang','alamat_cabang']},
            {
                model: model.nasabah,
                attributes: ['nama_nasabah','alamat_nasabah']
            },
            {
                model: model.transaksi,
                attributes: ['jenis_transaksi','tanggal','jumlah']
            }],
    };
    if (page) options.offset = page;
    if (row) options.offset = row
    const allRekening = await rekening.findAll(options);
        return res.status(200).json({
            status: 'Success',
            data: allRekening
        })
}

const getRekeningById = async (req, res) => {
    const options = {
        attributes: ['id', 'pin', 'cabang_bank_id', 'saldo'],
        include: [{
            model: model.cabang_bank,
            attributes: ['nama_cabang','alamat_cabang']},
            {
                model: model.nasabah,
                attributes: ['nama_nasabah','alamat_nasabah']
            },
            {
                model: model.transaksi,
                attributes: ['jenis_transaksi','tanggal','jumlah']
            }]
    }
    const {id} = req.params
    const cariRekening = await rekening.findByPk(id, options)
    if (cariRekening) {
        return res.status(200).json({
            status: 'Success',
            data: cariRekening
        })
    } else if (!cariRekening){
        return res.status(400).json({ 
            status: 'Error',
            message: 'id tidak ditemukan'
        })
    } 
}

const postRekening = async (req, res) => {
    const { pin, cabang_bank_id, saldo } = req.body
    const rekeningData = {
        pin: pin,
        cabang_bank_id: cabang_bank_id,
        saldo: saldo
    }
    const foundPin = await rekening.findOne({
        where: {
            pin: req.body.pin
        }
    })
    const foundCabangBank = await rekening.findOne({
        where: {
            cabang_bank_id: req.body.cabang_bank_id
        }
    })
    if (!foundPin && foundCabangBank) {
        const tambahRekening = await rekening.create(rekeningData)
        res.status(201).json({
            status: 'Success',
            data: rekeningData
        })
    }
    if (foundPin && !foundCabangBank) {
        res.status(400).json({
            status: 'Error',
            message: `Pin sudah digunakan dan Cabang Bank dengan id ${req.body.cabang_bank_id} tidak ditemukan.`
        })
    } else if (foundPin) {
        res.status(400).json({
            status: 'Error',
            message: 'Pin sudah digunakan'
        })
    } else if (!foundCabangBank) {
        res.status(400).json({
            status: 'Error',
            message: `Cabang Bank dengan id ${req.body.cabang_bank_id} tidak ditemukan`
        })
    }
}

const deleteRekening = async (req, res) => {
    const {id} = req.params
    const cariRekening = await rekening.findByPk(id)
    if (!cariRekening){
        return res.status(400).json({
            status: 'Error',
            message: `User dengan id ${req.params.id} tidak ditemukan`
        })
    }
    if (cariRekening) {
        const hapusRekening = await cariRekening.destroy()
        return res.status(200).json({
            status: 'Success',
            message: `User dengan id ${req.params.id} berhasil dihapus`
        })
    }
}

const putRekening = async (req, res) => {
    const {id} = req.params
    const {pin, cabang_bank_id, saldo} = req.body
    const cariRekening = await rekening.findOne({
        where: {
            id
        }
    })
    if(!cariRekening){
        res.status(400).json({
            status: 'error',
            message: `User dengan id ${req.params.id} tidak ditemukan`
        })
    }
    if (pin) {
        cariRekening.pin = pin
    } 
    if (cabang_bank_id) {
        cariRekening.cabang_bank_id = cabang_bank_id
    }
    if (saldo) {
        cariRekening.saldo = saldo
    }
    const updateRekening = await cariRekening.save()
    if (updateRekening) {
        res.status(200).json({
            status: 'success',
            data: updateRekening
        })
    } else {
        res.status(400).json({
            status: 'error',
            message: error
        })
    }
}
module.exports = {
    getAllRekening,
    getRekeningById,
    postRekening,
    deleteRekening,
    putRekening
}