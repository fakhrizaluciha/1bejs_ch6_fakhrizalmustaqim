const fs = require('fs');
const { nasabah_has_rekening } = require('../models')
const model = require('../models')

const getAllNasabahRekening = async (req, res) => {
    let { page, row } = req.query
    page -= 1
    const options = {
        attributes: ['id', 'nasabah_id', 'rekening_id'],
        include: [{
            model: model.rekening,
            attributes: ['pin','cabang_bank_id','saldo']},
            {
                model: model.nasabah,
                attributes: ['nama_nasabah','alamat_nasabah']
        }],
    }
    if (page) options.offset = page;
    if (row) options.offset = row
    const allNasabahRekening = await nasabah_has_rekening.findAll(options);
    return res.status(200).json({
        status: 'Success',
        data: allNasabahRekening
    })
}

const getNasabahRekeningById = async (req, res) => {
    const options = {
        attributes: ['id', 'nasabah_id', 'rekening_id'],
        include: [{
                model: model.rekening,
                attributes: ['pin','cabang_bank_id','saldo']},
            {
                model: model.nasabah,
                attributes: ['nama_nasabah','alamat_nasabah']
        }]
    }
    const {id} = req.params
    const cariNasabahRekening = await nasabah_has_rekening.findByPk(id, options)
    if (cariNasabahRekening) {
        return res.status(200).json({
            status: 'Success',
            data: cariNasabahRekening
        })
    } else {
        return res.status(400).json({ 
            status: 'Error',
            message: 'id tidak ditemukan'
        })
    } 
}

const postNasabahRekening = async (req, res) => {
    const { nasabah_id, rekening_id  } = req.body
    const nasabahRekeningData = {
        nasabah_id: nasabah_id,
        rekening_id: rekening_id,
    }
    const tambahTransaksi = await nasabah_has_rekening.create(nasabahRekeningData)
    res.status(201).json({
        status: 'Success',
        data: nasabahRekeningData
    })
}

const deleteNasabahRekening = async (req, res) => {
    const {id} = req.params
    const cariNasabahRekening = await nasabah_has_rekening.findByPk(id)
    if (!cariNasabahRekening) {
        res.status(400).json({
            status: 'Error',
            message: `User dengan id ${req.params.id} tidak ditemukan`
        })
    }
    if (cariNasabahRekening) {
        const hapusNasabahRekening = await cariNasabahRekening.destroy()
        res.status(200).json({
            status: 'Success',
            message: `User dengan id ${req.params.id} berhasil dihapus`
        })
    }
}

const putNasabahRekening = async (req, res) => {
    const {id} = req.params
    const { nasabah_id, rekening_id  } = req.body
    const cariNasabahRekening = await nasabah_has_rekening.findOne({
        where: {
            id
        }
    })
    if(!cariNasabahRekening){
        res.status(400).json({
            status: 'error',
            message: `User dengan id ${req.params.id} tidak ditemukan`
        })
    }
    if (nasabah_id) {
        cariNasabahRekening.nasabah_id = nasabah_id
    } 
    if (rekening_id) {
        cariNasabahRekening.rekening_id = rekening_id
    }
    const updateNasabahRekening = await cariNasabahRekening.save()
    if (updateNasabahRekening) {
        res.status(200).json({
            status: 'success',
            data: updateNasabahRekening
        })
    } else {
        res.status(400).json({
            status: 'error',
            message: error
        })
    }
}
module.exports = {
    getAllNasabahRekening,
    getNasabahRekeningById,
    postNasabahRekening,
    deleteNasabahRekening,
    putNasabahRekening
}