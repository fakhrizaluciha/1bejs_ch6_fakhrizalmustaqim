const fs = require('fs');
const { cabang_bank } = require('../models')
const model = require('../models')

const getAllCabangBank = async (req, res) => {
    let { page, row } = req.query
    page -= 1
    const options = {
        attributes: ['id', 'nama_cabang', 'alamat_cabang'],
        include: [{
            model: model.rekening,
            attributes: ['pin','cabang_bank_id','saldo']
        }],
    }
    if (page) options.offset = page;
    if (row) options.offset = row
    const allCabangBank = await cabang_bank.findAll(options);
        return res.status(200).json({
            status: 'Success',
            data: allCabangBank
        })
}

const getCabangBankById = async (req, res) => {
    const options = {
        attributes: ['id', 'nama_cabang', 'alamat_cabang'],
        include: [{
            model: model.rekening,
            attributes: ['pin','cabang_bank_id','saldo']
        }],
    }
    const {id} = req.params
    const cariCabangBank = await cabang_bank.findByPk(id, options)
    if (cariCabangBank) {
        return res.status(200).json({
            status: 'Success',
            data: cariCabangBank
        })
    } else {
        return res.status(400).json({ 
            status: 'Error',
            message: 'id tidak ditemukan'
        })
    } 
}

const postCabangBank = async (req, res) => {
    const { nama_cabang, alamat_cabang  } = req.body
    const cabangBankData = {
        nama_cabang: nama_cabang,
        alamat_cabang: alamat_cabang,
    }
    if (cabangBankData) {
        const tambahCabang = await cabang_bank.create(cabangBankData)
        return res.status(201).json({
        status: 'Success',
        data: cabangBankData
    })
    }
}

const deleteCabangank = async (req, res) => {
    const {id} = req.params
    const cariCabangBank = await cabang_bank.findByPk(id)
    if (!cariCabangBank) {
        return res.status(400).json({
            status: 'Error',
            message: `User dengan id ${req.params.id} tidak ditemukan`
        })
    }
    if (cariCabangBank) {
        const hapusCabangBank = await cariCabangBank.destroy()
        return res.status(200).json({
            status: 'Success',
            message: `User dengan id ${req.params.id} berhasil dihapus`
        })
    }
}

const putCabangBank = async (req, res) => {
    const {id} = req.params
    const {nama_cabang, alamat_cabang} = req.body
    const cariCabangBank = await cabang_bank.findOne({
        where: {
            id
        }
    })
    if(!cariCabangBank){
        res.status(400).json({
            status: 'Error',
            message: `User dengan id ${req.params.id} tidak ditemukan`
        })
    }
    if (nama_cabang) {
        cariCabangBank.nama_cabang = nama_cabang
    } 
    if (alamat_cabang) {
        cariCabangBank.alamat_cabang = alamat_cabang
    }
    const updateCabangBank = await cariCabangBank.save()
    if (updateCabangBank) {
        res.status(200).json({
            status: 'success',
            data: updateCabangBank
        })
    } else {
        res.status(400).json({
            status: 'error',
            message: error
        })
    }
}
module.exports = {
    getAllCabangBank,
    getCabangBankById,
    postCabangBank,
    deleteCabangank,
    putCabangBank
}