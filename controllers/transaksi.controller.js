const fs = require('fs');
const { transaksi } = require('../models')
const model = require('../models')

const getAllTransaksi = async (req, res) => {
    let { page, row } = req.query
    page -= 1
    const options = {
        attributes: ['id', 'nasabah_id', 'rekening_id', 'jenis_transaksi', 'tanggal', 'jumlah'],
        include: [{
            model: model.nasabah,
            attributes: ['nama_nasabah','alamat_nasabah']},
            {
                model: model.rekening,
                attributes: ['pin','cabang_bank_id','saldo']
        }]
    }
    if (page) options.offset = page;
    if (row) options.offset = row
    const allTransaksi = await transaksi.findAll(options);
    return res.status(200).json({
        status: 'Success',
        data: allTransaksi
    })
}

const getTransaksiById = async (req, res) => {
    const options = {
        attributes: ['id', 'nasabah_id', 'rekening_id', 'jenis_transaksi', 'tanggal', 'jumlah'],
        include: [{
            model: model.nasabah,
            attributes: ['nama_nasabah','alamat_nasabah']},
            {
                model: model.rekening,
                attributes: ['pin','cabang_bank_id','saldo']
        }]
    }
    const {id} = req.params
    const cariUser = await transaksi.findByPk(id, options)
    if (cariUser) {
        return res.status(200).json({
            status: 'Success',
            data: cariUser
        })
    } else if (!cariUser){
        return res.status(400).json({ 
            status: 'Error',
            message: 'id tidak ditemukan'
        })
    } 
}

const postTransaksi = async (req, res) => {
    const { nasabah_id, rekening_id, jenis_transaksi, tanggal, jumlah } = req.body
    const transaksiData = {
        nasabah_id: nasabah_id,
        rekening_id: rekening_id,
        jenis_transaksi: jenis_transaksi,
        tanggal: tanggal,
        jumlah: jumlah
    }
    const foundNasabah = await transaksi.findOne({
        where: {
            id: req.body.nasabah_id
        }
    })
    const foundRekening = await transaksi.findOne({
        where: {
            id: req.body.rekening_id
        }
    })
    if (foundNasabah && foundRekening) {
        const tambahTransaksi = await transaksi.create(transaksiData)
        res.status(201).json({
            status: 'Success',
            data: transaksiData
        })
    }
    if (!foundRekening && !foundNasabah) {
        res.status(400).json({
            status: 'Error',
            message: `Rekening dan Nasabah Id dengan nomor ${req.body.rekening_id} dan ${req.body.nasabah_id} tidak ditemukan.`
        })
    } else if (!foundNasabah) {
        res.status(400).json({
            status: 'Error',
            message: `Nasabah Id dengan nomor ${req.body.nasabah_id} tidak ditemukan.`
        })
    } else if (!foundRekening) {
        res.status(400).json({
            status: 'Error',
            message: `Rekening Id dengan nomor ${req.body.rekening_id} tidak ditemukan.`
        })
    }
}

const deleteTransaksi = async (req, res) => {
    const {id} = req.params
    const cariTransaksi = await transaksi.findByPk(id)
    if (!cariTransaksi) {
        return res.status(400).json({
            status: 'Error',
            message: `User dengan id ${req.params.id} tidak ditemukan`
        })
    }
    if (cariTransaksi) {
        const hapusTransaksi = await cariTransaksi.destroy()
        return res.status(200).json({
            status: 'Success',
            message: `User dengan id ${req.params.id} berhasil dihapus`
        })
    }
}

const putTransaksi = async (req, res) => {
    const {id} = req.params
    const { nasabah_id, rekening_id, jenis_transaksi, tanggal, jumlah,  } = req.body
    const cariTransaksi = await transaksi.findOne({
        where: {
            id
        }
    })
    if(!cariTransaksi){
        res.status(400).json({
            status: 'error',
            message: `User dengan id ${req.params.id} tidak ditemukan`
        })
    }
    if (nasabah_id) {
        cariTransaksi.nasabah_id = nasabah_id
    } 
    if (rekening_id) {
        cariTransaksi.rekening_id = rekening_id
    }
    if (jenis_transaksi) {
        cariTransaksi.jenis_transaksi = jenis_transaksi
    }
    if (tanggal) {
        cariTransaksi.tanggal = tanggal
    }
    if (jumlah) {
        cariTransaksi.jumlah = jumlah
    }
    const updateTransaksi = await cariTransaksi.save()
    if (updateTransaksi) {
        res.status(200).json({
            status: 'success',
            data: updateTransaksi
        })
    } else {
        res.status(400).json({
            status: 'error',
            message: error
        })
    }
}
module.exports = {
    getAllTransaksi,
    getTransaksiById,
    postTransaksi,
    deleteTransaksi,
    putTransaksi
}