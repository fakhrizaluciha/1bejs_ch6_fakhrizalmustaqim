const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const fs = require("fs");
const { nasabah } = require("../models");
const model = require("../models");
const checkPassword = require("../misc/auth");
const options = require("../routes/nasabah.route");
const salt = bcrypt.genSaltSync(10);
const { sendEmail, sendOTP } = require("../misc/mailer");

const getAllNasabah = async (req, res) => {
  let { page, row } = req.query;
  page -= 1;
  const options = {
    attributes: ["id", "nama_nasabah", "alamat_nasabah", "video"],
    include: [
      {
        model: model.transaksi,
        attributes: ["jenis_transaksi", "tanggal", "jumlah"],
      },
      {
        model: model.rekening,
        attributes: ["pin", "cabang_bank_id", "saldo"],
      },
    ],
  };
  if (page) options.offset = page;
  if (row) options.offset = row;
  const allNasabah = await nasabah.findAll(options);
  return res.status(200).json({
    status: "Success",
    data: allNasabah,
  });
};

const getNasabahById = async (req, res) => {
  const options = {
    attributes: ["id", "nama_nasabah", "alamat_nasabah", "video"],
    include: [
      {
        model: model.transaksi,
        attributes: ["jenis_transaksi", "tanggal", "jumlah"],
      },
      {
        model: model.rekening,
        attributes: ["pin", "cabang_bank_id", "saldo"],
      },
    ],
  };
  const { id } = req.params;
  const cariUser = await nasabah.findByPk(id, options);
  if (cariUser) {
    return res.status(200).json({
      status: "Success",
      data: cariUser,
    });
  } else if (!cariUser) {
    return res.status(400).json({
      status: "Error",
      message: `User dengan id ${req.params.id} tidak ditemukan`,
    });
  }
};

const register = async (req, res) => {
  const { email, nama_nasabah, alamat_nasabah, password, video_url } = req.body;
  const userData = {
    email: email,
    nama_nasabah: nama_nasabah,
    alamat_nasabah: alamat_nasabah,
    password: password,
    video: video_url,
  };
  const foundName = await nasabah.findOne({
    where: {
      nama_nasabah: req.body.nama_nasabah,
    },
  });
  const foundPassword = await nasabah.findOne({
    where: {
      password: req.body.password,
    },
  });
  if (!foundName && !foundPassword) {
    const kirimEmail = await sendEmail(userData.email, userData.nama_nasabah);
    const tambahNasabah = await nasabah.create(userData);
    return res.status(201).json({
      status: "Success",
      data: {
        nama_nasabah: userData.nama_nasabah,
        alamat_nasabah: userData.alamat_nasabah,
        video: userData.video,
      },
    });
  }
  if (foundName && foundPassword) {
    return res.status(400).json({
      status: "Error",
      message: "Nama dan password sudah dipakai",
    });
  } else if (foundName) {
    return res.status(400).json({
      status: "Error",
      message: "Nama sudah dipakai",
    });
  } else if (foundPassword) {
    return res.status(400).json({
      status: "Error",
      message: "Password sudah dipakai",
    });
  }
};

const deleteNasabah = async (req, res) => {
  const { id } = req.params;
  const cariUser = await nasabah.findByPk(id);
  if (!cariUser) {
    return res.status(400).json({
      status: "Error",
      message: `User dengan id ${req.params.id} tidak ditemukan`,
    });
  } else if (cariUser) {
    const hapusUser = await cariUser.destroy();
    return res.status(200).json({
      status: "Success",
      message: `User dengan id ${req.params.id} berhasil dihapus`,
    });
  }
};

const putNasabah = async (req, res) => {
  const { id } = req.params;
  const { nama_nasabah, alamat_nasabah } = req.body;
  const cariUser = await nasabah.findOne({
    where: {
      id,
    },
  });
  if (!cariUser) {
    return res.status(400).json({
      status: "Error",
      message: `User dengan id ${req.params.id} tidak ditemukan`,
    });
  }
  if (nama_nasabah) {
    cariUser.nama_nasabah = nama_nasabah;
  }
  if (alamat_nasabah) {
    cariUser.alamat_nasabah = alamat_nasabah;
  }
  const updateNasabah = await cariUser.save();
  if (updateNasabah) {
    return res.status(200).json({
      status: "Success",
      data: updateNasabah,
    });
  } else {
    return res.status(400).json({
      status: "Error",
      message: error,
    });
  }
};

const login = async (req, res) => {
  const { email, password } = req.body;
  const foundPassword = await nasabah.findOne({
    where: {
      email: email,
    },
  });
  const validasi = bcrypt.compareSync(password, foundPassword.password);
  if (validasi) {
    const userData = {
      id: foundPassword.id,
      email: foundPassword.email,
    };
    var token = jwt.sign(userData, "secret", { expiresIn: "12h" });
    return res.status(201).json({
      Token: token,
    });
  }
  return res.status(400).json({
    status: "Tidak terdaftar",
    message: "Coba lagi",
  });
};

const getOtp = async (req, res) => {
  try {
    const { email } = req.body;
    const otp = Math.random().toString(36).slice(-6).toUpperCase();
    const hashOtp = bcrypt.hashSync(otp, +salt);
    const updateEmail = await nasabah.update(
      {
        otp: hashOtp,
      },
      {
        where: {
          email: email,
        },
      }
    );
    if (!updateEmail[0]) {
      return res.status(400).json({
        status: "Error",
        message: "Email tidak terdaftar",
      });
    }
    const otpMessage = `OTP: ${otp}
                            BUKAN KODE PROMO, JANGAN BAGIKAN OTP!`;
    const emailRes = await sendOTP(email, otpMessage);

    return res.status(200).json({
      message: `Kode unik sukses terkirim ke ${emailRes.accepted
        .join(",")
        .split(",")}`,
    });
  } catch (error) {
    if (error.code) {
      return res.status(error.code).json({
        status: error.status,
        message: error.message,
      });
    }
    next(error);
  }
};

const sendToEmail = async (req, res) => {
  try {
    const { to, message } = req.body;
    const emailRes = await sendEmail(to, message);
    return res.status(200).json({
      message: `Email sukses dikirim ke ${emailRes.accepted
        .join(",")
        .split(",")}`,
    });
  } catch (error) {
    return res.status(400).json({
      status: "Error",
      message: error,
    });
  }
};

const PasswordOTP = async (req, res, next) => {
  try {
    const { email, newPassword, newPasswordConfirmation, otp } = req.body;

    if (newPassword === newPasswordConfirmation) {
      const foundUser = await nasabah.findOne({
        where: {
          email: email,
        },
      });
      if (!foundUser)
        throw {
          message: "Email is not registered",
          status: "Failed",
          code: 400,
        };

      const compareOtp = checkPassword(otp, foundUser.otp);

      if (compareOtp) {
        await foundUser.update({
          password: bcrypt.hashSync(newPassword, +salt),
          otp: null,
        });

        return res.status(200).json({
          status: "Success",
          message: "Password sukses diganti",
        });
      }

      throw { message: "Wrong OTP", status: "Failed", code: 400 };
    }

    throw { message: `Password doesn't match`, status: "Failed", code: 400 };
  } catch (error) {
    if (error.code) {
      return res.status(error.code).json({
        status: error.status,
        message: error.message,
      });
    }
    next(error);
  }
};

module.exports = {
  getAllNasabah,
  getNasabahById,
  register,
  deleteNasabah,
  putNasabah,
  login,
  getOtp,
  sendToEmail,
  PasswordOTP,
};
