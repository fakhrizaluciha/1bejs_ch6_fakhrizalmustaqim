const request = require('supertest');
const app = require('../app')
const mock = jest.fn()
const mockRequest = (body = {}) => ({body})
const mockResponse = () => {
    const res = {}
    res.json = mock.mockReturnValue(res)
    res.status  = mock.mockReturnValue(res)
    return res
}

describe('GET /v1/nasabahRekening', () => {
    it('get all data', async () => {
        const response = await request(app).get('/v1/nasabahRekening');
        const {status, data} = response.body
        expect(response.status).toBe(200);
        expect(status).toBe('Success')
        expect(data).toMatchObject(response.body.data)
    })
})
describe('GET /v1/nasabahRekening/:id', () => {
    it('get nasabahRekening by Id', async () => {
        const response = await request(app).get('/v1/nasabahRekening/2')
        const {status, data} = response.body
        expect(response.error).toBe(false)
        expect(response.status).toBe(200);
        expect(status).toBe('Success')
        expect(data).toMatchObject(response.body.data)
    })
})
describe('GET /v1/nasabahRekening/:id', () => {
    it('wrong get nasabahRekening by Id', async () => {
        const response = await request(app).get('/v1/nasabahRekening/20')
        const {status, message} = response.body
        expect(response.status).toBe(400);
        expect(status).toBe('Error')
        expect(message).toBe('id tidak ditemukan')
    })
})
describe('POST /v1/nasabahRekening', () => {
    it('post', async () => {
        const response = await request(app).post('/v1/nasabahRekening')
        .send({
            nasabah_id: 3,
            rekening_id: 2,   
        })
        const {status, data} = response.body
        expect(response.error).toBe(false)
        expect(response.status).toBe(201);
        expect(status).toBe('Success')
        expect(data).toMatchObject(response.body.data)
    })
})
describe('DELETE /v1/nasabahRekening/:id', () => {
    it('delete', async () => {
        const response = await request(app).delete('/v1/nasabahRekening/1')
        const {status, message} = response.body
        expect(response.error).toBe(false)
        expect(response.status).toBe(200);
        expect(status).toBe('Success')
        expect(message).toBe(response.body.message)
    })
})
describe('DELETE /v1/nasabahRekening/:id', () => {
    it('wrong delete', async () => {
        const response = await request(app).delete('/v1/nasabahRekening/20')
        const {status, message} = response.body
        expect(response.status).toBe(400);
        expect(status).toBe('Error')
        expect(message).toBe(response.body.message)
    })
})