const request = require('supertest');
const app = require('../app')
const mock = jest.fn()
const mockRequest = (body = {}) => ({body})
const mockResponse = () => {
    const res = {}
    res.json = mock.mockReturnValue(res)
    res.status  = mock.mockReturnValue(res)
    return res
}

describe('GET /v1/rekening', () => {
    it('get all data', async () => {
        const response = await request(app).get('/v1/rekening');
        const {status, data} = response.body
        expect(response.status).toBe(200);
        expect(status).toBe('Success')
        expect(data).toMatchObject(response.body.data)
    })
})
describe('GET /v1/rekening/:id', () => {
    it('get rekening by Id', async () => {
        const response = await request(app).get('/v1/rekening/2')
        const {status, data} = response.body
        expect(response.error).toBe(false)
        expect(response.status).toBe(200);
        expect(status).toBe('Success')
        expect(data).toMatchObject(response.body.data)
    })
})
describe('GET /v1/rekening/:id', () => {
    it('wrong get rekening by Id', async () => {
        const response = await request(app).get('/v1/rekening/20')
        const {status, message} = response.body
        expect(response.status).toBe(400);
        expect(status).toBe('Error')
        expect(message).toBe(response.body.message)
    })
})
describe('POST /v1/rekening', () => {
    it('post', async () => {
        const response = await request(app).post('/v1/rekening')
        .send({
            pin: 10101,
            cabang_bank_id: 2,
            saldo: 1200000
        })
        const {status, data} = response.body
        expect(response.error).toBe(false)
        expect(response.status).toBe(201);
        expect(status).toBe('Success')
        expect(data).toMatchObject(response.body.data)
    })
})
describe('POST /v1/rekening', () => {
    it('wrong post cabang bank id', async () => {
        const response = await request(app).post('/v1/rekening')
        .send({
            pin: 1112,
            cabang_bank_id: 20,
            saldo: 1200000
        })
        const {status, message} = response.body
        expect(response.status).toBe(400);
        expect(status).toBe('Error')
        expect(message).toBe(response.body.message)
    })
})
describe('POST /v1/rekening', () => {
    it('wrong post pin', async () => {
        const response = await request(app).post('/v1/rekening')
        .send({
            pin: 1111,
            cabang_bank_id: 2,
            saldo: 1200000
        })
        const {status, message} = response.body
        expect(response.status).toBe(400);
        expect(status).toBe('Error')
        expect(message).toBe(response.body.message)
    })
})
describe('POST /v1/rekening', () => {
    it('wrong post pin dan cabang bank', async () => {
        const response = await request(app).post('/v1/rekening')
        .send({
            pin: 1111,
            cabang_bank_id: 20,
            saldo: 1200000
        })
        const {status, message} = response.body
        expect(response.status).toBe(400);
        expect(status).toBe('Error')
        expect(message).toBe(response.body.message)
    })
})
describe('DELETE /v1/rekening/:id', () => {
    it('delete', async () => {
        const response = await request(app).delete('/v1/rekening/1')
        const {status, message} = response.body
        expect(response.error).toBe(false)
        expect(response.status).toBe(200);
        expect(status).toBe('Success')
        expect(message).toBe(response.body.message)
    })
})
describe('DELETE /v1/rekening/:id', () => {
    it('wrong delete', async () => {
        const response = await request(app).delete('/v1/rekening/20')
        const {status, message} = response.body
        expect(response.status).toBe(400);
        expect(status).toBe('Error')
        expect(message).toBe(response.body.message)
    })
})