const request = require('supertest');
const app = require('../app')
const mock = jest.fn()
const mockRequest = (body = {}) => ({body})
const mockResponse = () => {
    const res = {}
    res.json = mock.mockReturnValue(res)
    res.status  = mock.mockReturnValue(res)
    return res
}

describe('GET /v1/nasabah', () => {
    it('get all data', async () => {
        const response = await request(app).get('/v1/nasabah');
        const {status, data} = response.body
        expect(response.status).toBe(200);
        expect(status).toBe('Success')
        expect(data).toMatchObject(response.body.data)
    })
})
describe('GET /v1/nasabah/:id', () => {
    it('get nasabah by Id', async () => {
        const response = await request(app).get('/v1/nasabah/2')
        const {status, data} = response.body
        expect(response.error).toBe(false)
        expect(response.status).toBe(200);
        expect(status).toBe('Success')
        expect(data).toMatchObject(response.body.data)
    })
})
describe('GET /v1/nasabah/:id', () => {
    it('wrong get nasabah by Id', async () => {
        const response = await request(app).get('/v1/nasabah/20')
        const {status, message} = response.body
        expect(response.status).toBe(400);
        expect(status).toBe('Error')
        expect(message).toBe(response.body.message)
    })
})
describe('POST /v1/nasabah', () => {
    it('post', async () => {
        const response = await request(app).post('/v1/nasabah')
        .send({
            nama_nasabah:  'Rizal',
            alamat_nasabah: 'Semarang'
        })
        const {status, data} = response.body
        expect(response.error).toBe(false)
        expect(response.status).toBe(201);
        expect(status).toBe('Success')
        expect(data).toMatchObject(response.body.data)
    })
})
describe('POST /v1/nasabah', () => {
    it('wrong post', async () => {
        const response = await request(app).post('/v1/nasabah')
        .send({
            nama_nasabah:  'taqim',
            alamat_nasabah: 'Semarang'
        })
        const {status, message} = response.body
        expect(response.status).toBe(400);
        expect(status).toBe('Error')
        expect(message).toBe(response.body.message)
    })
})
describe('DELETE /v1/nasabah/:id', () => {
    it('delete', async () => {
        const response = await request(app).delete('/v1/nasabah/1')
        const {status, message} = response.body
        expect(response.error).toBe(false)
        expect(response.status).toBe(200);
        expect(status).toBe('Success')
        expect(message).toBe(response.body.message)
    })
})
describe('DELETE /v1/nasabah/:id', () => {
    it('wrong delete', async () => {
        const response = await request(app).delete('/v1/nasabah/19')
        const {status, message} = response.body
        expect(response.status).toBe(400);
        expect(status).toBe('Error')
        expect(message).toBe(response.body.message)
    })
})