const request = require('supertest');
const app = require('../app')
const mock = jest.fn()
const mockRequest = (body = {}) => ({body})
const mockResponse = () => {
    const res = {}
    res.json = mock.mockReturnValue(res)
    res.status  = mock.mockReturnValue(res)
    return res
}

describe('GET /v1/cabangBank', () => {
    it('get all data', async () => {
        const response = await request(app).get('/v1/cabangBank');
        const {status, data} = response.body
        expect(response.status).toBe(200);
        expect(status).toBe('Success')
        expect(data).toMatchObject(response.body.data)
    })
})
describe('GET /v1/cabangBank/:id', () => {
    it('get cabang bank by Id', async () => {
        const response = await request(app).get('/v1/cabangBank/4')
        const {status, data} = response.body
        expect(response.error).toBe(false)
        expect(response.status).toBe(200);
        expect(status).toBe('Success')
        expect(data).toMatchObject(response.body.data)
    })
})
describe('GET /v1/cabangBank/:id', () => {
    it('wrong get cabangBank by Id', async () => {
        const response = await request(app).get('/v1/cabangBank/20')
        const {status, message} = response.body
        expect(response.status).toBe(400);
        expect(status).toBe('Error')
        expect(message).toBe(response.body.message)
    })
})
describe('POST /v1/cabangBank', () => {
    it('post', async () => {
        const response = await request(app).post('/v1/cabangBank')
        .send({
            nama_cabang: "nama_cabang",
            alamat_cabang: "alamat_cabang",
        })
        const {status, data} = response.body
        expect(response.error).toBe(false)
        expect(response.status).toBe(201);
        expect(status).toBe('Success')
        expect(data).toMatchObject(response.body.data)
    })
})
describe('DELETE /v1/cabangBank/:id', () => {
    it('delete', async () => {
        const response = await request(app).delete('/v1/cabangBank/5')
        const {status, message} = response.body
        expect(response.error).toBe(false)
        expect(response.status).toBe(200);
        expect(status).toBe('Success')
        expect(message).toBe(response.body.message)
    })
})
describe('DELETE /v1/cabangBank/:id', () => {
    it('wrong delete', async () => {
        const response = await request(app).delete('/v1/cabangBank/20')
        const {status, message} = response.body
        expect(response.status).toBe(400);
        expect(status).toBe('Error')
        expect(message).toBe(response.body.message)
    })
})