const request = require('supertest');
const app = require('../app')
const mock = jest.fn()
const mockRequest = (body = {}) => ({body})
const mockResponse = () => {
    const res = {}
    res.json = mock.mockReturnValue(res)
    res.status  = mock.mockReturnValue(res)
    return res
}

describe('GET /v1/transaksi', () => {
    it('get all data', async () => {
        const response = await request(app).get('/v1/transaksi');
        const {status, data} = response.body
        expect(response.status).toBe(200);
        expect(status).toBe('Success')
        expect(data).toMatchObject(response.body.data)
    })
})
describe('GET /v1/transaksi/:id', () => {
    it('get transaksi by Id', async () => {
        const response = await request(app).get('/v1/transaksi/2')
        const {status, data} = response.body
        expect(response.error).toBe(false)
        expect(response.status).toBe(200);
        expect(status).toBe('Success')
        expect(data).toMatchObject(response.body.data)
    })
})
describe('GET /v1/transaksi/:id', () => {
    it('wrong get transaksi by Id', async () => {
        const response = await request(app).get('/v1/transaksi/20')
        const {status, message} = response.body
        expect(response.status).toBe(400);
        expect(status).toBe('Error')
        expect(message).toBe(response.body.message)
    })
})
describe('POST /v1/transaksi', () => {
    it('post', async () => {
        const response = await request(app).post('/v1/transaksi')
        .send({
            nasabah_id: 2,
            rekening_id: 2,
            jenis_transaksi: 'debit',
            tanggal: "2022-05-11T06:37:14.720Z",
            jumlah: 2500000
        })
        const {status, data} = response.body
        expect(response.error).toBe(false)
        expect(response.status).toBe(201);
        expect(status).toBe('Success')
        expect(data).toMatchObject(response.body.data)
    })
})
describe('POST /v1/transaksi', () => {
    it('wrong post nasabah_id', async () => {
        const response = await request(app).post('/v1/transaksi')
        .send({
            nasabah_id: 20,
            rekening_id: 2,
            jenis_transaksi: 'debit',
            tanggal: "2022-05-11T06:37:14.720Z",
            jumlah: 2500000
        })
        const {status, message} = response.body
        expect(response.status).toBe(400);
        expect(status).toBe('Error')
        expect(message).toBe(response.body.message)
    })
})
describe('POST /v1/transaksi', () => {
    it('wrong post rekening_id', async () => {
        const response = await request(app).post('/v1/transaksi')
        .send({
            nasabah_id: 2,
            rekening_id: 20,
            jenis_transaksi: 'debit',
            tanggal: "2022-05-11T06:37:14.720Z",
            jumlah: 2500000
        })
        const {status, message} = response.body
        expect(response.status).toBe(400);
        expect(status).toBe('Error')
        expect(message).toBe(response.body.message)
    })
})
describe('POST /v1/transaksi', () => {
    it('wrong post rekening_id and nasabah_id', async () => {
        const response = await request(app).post('/v1/transaksi')
        .send({
            nasabah_id: 200,
            rekening_id: 200,
            jenis_transaksi: 'debit',
            tanggal: "2022-05-11T06:37:14.720Z",
            jumlah: 2500000
        })
        const {status, message} = response.body
        expect(response.status).toBe(400);
        expect(status).toBe('Error')
        expect(message).toBe(response.body.message)
    })
})
describe('DELETE /v1/transaksi/:id', () => {
    it('delete', async () => {
        const response = await request(app).delete('/v1/transaksi/1')
        const {status, message} = response.body
        expect(response.error).toBe(false)
        expect(response.status).toBe(200);
        expect(status).toBe('Success')
        expect(message).toBe(response.body.message)
    })
})
describe('DELETE /v1/transaksi/:id', () => {
    it('wrong delete', async () => {
        const response = await request(app).delete('/v1/transaksi/20')
        const {status, message} = response.body
        expect(response.status).toBe(400);
        expect(status).toBe('Error')
        expect(message).toBe(response.body.message)
    })
})