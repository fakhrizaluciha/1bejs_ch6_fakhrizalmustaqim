'use strict';
const transaksiData = require('../masterData/transaksi.json')
module.exports = {
  async up (queryInterface, Sequelize) {
    const insert = transaksiData.map((eachTransaksi) => {
      eachTransaksi.createdAt = new Date();
      eachTransaksi.updatedAt = new Date();
      return eachTransaksi
    })
    await queryInterface.bulkInsert('transaksis', transaksiData)
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('transaksis', null)
  }
};
