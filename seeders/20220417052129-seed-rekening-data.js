'use strict';
const rekeningData = require('../masterData/rekening.json')
module.exports = {
  async up (queryInterface, Sequelize) {
    const insert = rekeningData.map((eachRekening) => {
      eachRekening.createdAt = new Date();
      eachRekening.updatedAt = new Date();
      return eachRekening
    })
    await queryInterface.bulkInsert('rekenings', rekeningData)
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('rekenings', null)
  }
};
