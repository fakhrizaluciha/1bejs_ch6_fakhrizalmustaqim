'use strict';
const cabangBankData = require('../masterData/cabang_bank.json')
module.exports = {
  async up (queryInterface, Sequelize) {
    const insert = cabangBankData.map((eachCabangBank) => {
      eachCabangBank.createdAt = new Date();
      eachCabangBank.updatedAt = new Date();
      return eachCabangBank
    })
    await queryInterface.bulkInsert('cabang_banks', cabangBankData)
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('cabang_banks', null)
  }
};
