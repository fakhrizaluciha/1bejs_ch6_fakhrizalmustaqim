'use strict';
const nasabahData = require('../masterData/nasabah.json')

module.exports = {
  async up (queryInterface, Sequelize) {
    const insert = nasabahData.map((eachNasabah) => {
      eachNasabah.createdAt = new Date();
      eachNasabah.updatedAt = new Date();
      return eachNasabah
    })
    await queryInterface.bulkInsert('nasabahs', nasabahData)
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('nasabahs', null)
  }
};
