'use strict';
const nasabahRekeningData = require('../masterData/nasabah_has_rekening.json')
module.exports = {
  async up (queryInterface, Sequelize) {
    const insert = nasabahRekeningData.map((eachNasabahRekening) => {
      eachNasabahRekening.createdAt = new Date();
      eachNasabahRekening.updatedAt = new Date();
      return eachNasabahRekening
    })
    await queryInterface.bulkInsert('nasabah_has_rekenings', nasabahRekeningData)
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('nasabah_has_rekenings', null)
  }
};
