const express = require('express')
const router = express.Router()
const { getAllCabangBank, postCabangBank, deleteCabangank, getCabangBankById, putCabangBank } = require('../controllers/cabangBank.controllers')
const restrict = require('../misc/passport')

router.get('/', restrict, getAllCabangBank)
router.get('/:id', restrict, getCabangBankById)
router.post('/', restrict, postCabangBank)
router.put('/:id', restrict, putCabangBank)
router.delete('/:id', restrict, deleteCabangank)

module.exports = router;