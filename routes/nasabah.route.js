const express = require('express')
const router = express.Router()
const Nasabah = require('../controllers/nasabah.controllers')
const restrict = require('../misc/passport')
const uploadVideo = require('../misc/multerVideo')
const uploadWithCloudinary = require('../misc/cloudinary')
const mailer = require('../misc/mailer')

router.get('/', restrict, Nasabah.getAllNasabah)
router.get('/:id', restrict, Nasabah.getNasabahById)
router.post('/register', uploadVideo.single('video'), uploadWithCloudinary, Nasabah.register)
router.delete('/:id', restrict, Nasabah.deleteNasabah)
router.put('/:id', restrict, Nasabah.putNasabah)
router.post('/login', Nasabah.login)
router.post('/get-otp', Nasabah.getOtp)
router.post('/send-email', Nasabah.sendToEmail)
router.post('/new-password', Nasabah.PasswordOTP)

module.exports = router;