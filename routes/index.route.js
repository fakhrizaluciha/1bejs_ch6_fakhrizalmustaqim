const express = require('express')
const router = express.Router();
const nasabah = require('./nasabah.route');
const transaksi = require('./transaksi.route')
const cabangBank = require('./cabangBank.route')
const rekening = require('./rekening.route')
const nasabahRekening = require('./nasabah_rekening.route')

router.use('/nasabah' , nasabah)
router.use('/transaksi', transaksi)
router.use('/cabangBank', cabangBank)
router.use('/rekening', rekening)
router.use('/nasabahRekening', nasabahRekening)

module.exports = router;    