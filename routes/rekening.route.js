const express = require('express')
const router = express.Router()
const { postRekening, getAllRekening, deleteRekening, getRekeningById, putRekening } = require('../controllers/rekening.controllers')
const restrict = require('../misc/passport')

router.get('/', restrict, getAllRekening)
router.get('/:id', restrict, getRekeningById)
router.post('/', restrict, postRekening)
router.delete('/:id', restrict, deleteRekening)
router.put('/:id', restrict, putRekening)

module.exports = router;