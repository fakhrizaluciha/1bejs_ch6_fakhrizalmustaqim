const express = require('express')
const router = express.Router()
const {getAllTransaksi, getTransaksiById, postTransaksi, deleteTransaksi, putTransaksi} = require('../controllers/transaksi.controller')
const restrict = require('../misc/passport')

router.get('/', restrict, getAllTransaksi)
router.get('/:id', restrict, getTransaksiById)
router.post('/', restrict, postTransaksi)
router.delete('/:id', restrict, deleteTransaksi)
router.put('/:id', restrict, putTransaksi)

module.exports = router;