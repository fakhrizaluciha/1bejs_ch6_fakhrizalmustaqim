const express = require('express')
const router = express.Router()
const { getAllNasabahRekening, getNasabahRekeningById, postNasabahRekening, deleteNasabahRekening, putNasabahRekening } = require('../controllers/nasabah_rekening.controller')
const restrict = require('../misc/passport')

router.get('/', restrict, getAllNasabahRekening)
router.get('/:id', restrict, getNasabahRekeningById)
router.post('/', restrict, postNasabahRekening)
router.delete('/:id', restrict, deleteNasabahRekening)
router.put('/:id', restrict, putNasabahRekening)

module.exports = router;