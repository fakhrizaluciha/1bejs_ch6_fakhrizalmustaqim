const cloudinary = require('cloudinary').v2

cloudinary.config({ 
    cloud_name: 'dneaa4ocn', 
    api_key: '938551574458288', 
    api_secret: 'dYm8X3WWBtwWW49tyYucfsSnDM4' 
});

const uploadWithCloudinary = async (req, res, next) => {
    try {
        // console.log(req.file);
        const Result = await cloudinary.uploader.upload(req.file.path,
                {resource_type: "video", 
                public_id: "myfolder/mysubfolder/dog_closeup",
                chunk_size: 6000000,
                eager: [
                { width: 300, height: 300, crop: "pad", audio_codec: "none" }, 
                { width: 160, height: 100, crop: "crop", gravity: "south", audio_codec: "none" } ],                                   
                eager_async: true},
            function(error, result) {console.log(result, error)})
        // console.log(Result);
        req.body.video_url = Result.secure_url;
        // req.body.photo_url = Result.secure_url;
        next()
    } catch (error) {
        res.status(400).json({
            status: 'Error',
            message: error
        })
        // console.log(error);
    }
}

module.exports = uploadWithCloudinary