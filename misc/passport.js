const passport = require('passport')
const JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;
const opts = {}
const { nasabah } = require('../models')
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = 'secret';
// opts.issuer = 'accounts.examplesoft.com';
// opts.audience = 'yoursite.net';
passport.use(new JwtStrategy(opts, async (jwt_payload, done) => {
    nasabah.findOne({
        where: {
            id: jwt_payload.id,
            nama_nasabah: jwt_payload.nama_nasabah
        }
    })
    .then((nasabah) => done(null, nasabah))
    .catch((err) => done(err, false))
}));

module.exports = passport.authenticate('jwt', {session: false})