const nodemailer = require('nodemailer')

const sendEmail = (email, nama_nasabah) => {
    const mailOptions = {
        from: 'fakhrizaluciha@gmail.com',
        to: email,
        subject: 'Welcome!',
        text: `Hai ${nama_nasabah}, Selamat anda berhasil mendaftar`
    }
    const transporter = nodemailer.createTransport({
        host: 'smtp-relay.sendinblue.com',
        port: 587,
        auth: {
            user: 'fakhrizaluciha@gmail.com',
            pass: 'cmnYK4HP7IBhbGwk'
        }
    });
    return new Promise((resolve, reject) => {
        transporter.sendMail(mailOptions, (err, info) => {
            if (err) reject(err)
            else resolve(info)
        })
    })
}

const sendOTP = (receiver, messageToBeSent) => {
    const mailOptions = {
        from: 'fakhrizaluciha@gmail.com',
        to: `${receiver}`,
        subject: 'Penting!',
        text: `${messageToBeSent}`
    }
    const transporter = nodemailer.createTransport({
        host: 'smtp-relay.sendinblue.com',
        port: 587,
        auth: {
            user: 'fakhrizaluciha@gmail.com',
            pass: 'cmnYK4HP7IBhbGwk'
        }
    });
    return new Promise((resolve, reject) => {
        transporter.sendMail(mailOptions, (err, info) => {
            if (err) reject(err)
            else resolve(info)
        })
    })
}

module.exports = {
    sendEmail,
    sendOTP
}