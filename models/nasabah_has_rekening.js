'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class nasabah_has_rekening extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.rekening, {
        foreignKey: 'rekening_id',
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      })
      this.belongsTo(models.nasabah, {
        foreignKey: 'nasabah_id',
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      })
    }
  }
  nasabah_has_rekening.init({
    nasabah_id: DataTypes.INTEGER,
    rekening_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'nasabah_has_rekening',
  });
  return nasabah_has_rekening;
};