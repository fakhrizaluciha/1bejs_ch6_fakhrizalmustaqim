'use strict';
const bcrypt = require('bcrypt')
const salt = bcrypt.genSaltSync(10);
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class nasabah extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.transaksi, {
        foreignKey: 'nasabah_id',
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      })
      this.hasMany(models.nasabah_has_rekening, {
        foreignKey: 'nasabah_id',
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      })
      this.belongsToMany(models.rekening, {
        through: 'nasabah_has_rekening',
        foreignKey: 'nasabah_id',
        otherKey: 'rekening_id'
      })
    }
  }
  nasabah.init({
    email: DataTypes.STRING,
    nama_nasabah: DataTypes.STRING,
    alamat_nasabah: DataTypes.STRING,
    password: DataTypes.STRING,
    video: DataTypes.STRING,
    otp: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'nasabah',
    hooks: {
      beforeCreate: (nasabah, options) => {
        nasabah.password = bcrypt.hashSync(nasabah.password, salt);
        return nasabah
      }
    }
  });
  return nasabah;
};