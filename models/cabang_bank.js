'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class cabang_bank extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.rekening, {
        foreignKey: 'cabang_bank_id',
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      });
    }
  }
  cabang_bank.init({
    nama_cabang: DataTypes.STRING,
    alamat_cabang: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'cabang_bank',
  });
  return cabang_bank;
};