'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class rekening extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.transaksi, {
        foreignKey: 'rekening_id',
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      })
      this.belongsTo(models.cabang_bank, {
        foreignKey: 'cabang_bank_id'
      })
      this.hasMany(models.nasabah_has_rekening, {
        foreignKey: 'rekening_id',
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE'
      })
      this.belongsToMany(models.nasabah, {
        through: 'nasabah_has_rekening',
        foreignKey: 'rekening_id',
        otherKey: 'nasabah_id'
      })
    }
  }
  rekening.init({
    pin: DataTypes.STRING,
    cabang_bank_id: DataTypes.INTEGER,
    saldo: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'rekening',
  });
  return rekening;
};