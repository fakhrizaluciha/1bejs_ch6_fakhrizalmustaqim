'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class transaksi extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.nasabah, {
        foreignKey: 'nasabah_id'
      })
      this.belongsTo(models.rekening, {
        foreignKey: 'rekening_id'
      })
    }
  }
  transaksi.init({
    nasabah_id: DataTypes.INTEGER,
    rekening_id: DataTypes.INTEGER,
    jenis_transaksi: DataTypes.STRING,
    tanggal: DataTypes.DATE,
    jumlah: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'transaksi',
  });
  return transaksi;
};