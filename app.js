const express = require('express')
const morgan = require('morgan')
const app = express()
const router = require('./routes/index.route')
// const swaggerJSON = require('./api-documentation/swagger.json')
// const swaggerUI = require('swagger-ui-express')

app.use(express.json())
app.use(express.urlencoded({extended: true}))   
app.use(morgan('tiny'))

// app.use('/docs', swaggerUI.serve, swaggerUI.setup(swaggerJSON))
app.use('/v1', router)

app.get('/', (req, res) => {
    res.status(200).json({
        status: 'Success',
        message: 'Welcome'
    })
});

module.exports = app;